# Asus UX390UA Kernel patches

Asus Zenbook 3 UX390UA

## HD-audio patch

Laptop has Realtek ALC295 codec with tweaks from "Harman Kardon" or some other marketing anything. 

In vanilla kernel speakers always have max volume.

One of possible solutions is [edit pulseaudio config](https://wiki.archlinux.org/index.php/ASUS_Zenbook_UX390).
But on my laptop this cause crackling sounds.

This patch just override connection list on 0x17 pin.
I don't know is this right solution.
But patch doing the job.
Pulseaudio works well with default config on ArchLinux.
